package com.citi.training.trades.model;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;
import org.junit.Test;


import com.citi.training.trades.model.Trade;

public class TradeTests {
	
	private int testId = 75;
	private String testStock = "APPLE";
	private double testPrice = 300.50;
	private int testVolume = 400;
	
	@Test
	public void test_Trade_constructor() {
		
		Trade testTrade = new Trade(testId,testStock, testPrice, testVolume);
		assertEquals(testId, testTrade.getId());
		assertEquals(testStock, testTrade.getStock()); 
		assertEquals(testPrice, testTrade.getPrice(), 0.00001);
		assertEquals(testVolume, testTrade.getVolume());
	}
	
	@Test
	public void test_Trade_toString() {
		String testString = new Trade(testId, testStock, testPrice, testVolume).toString();
	
		assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testStock));
        assertTrue(testString.contains(String.valueOf(testPrice)));
        assertTrue(testString.contains((new Integer(testVolume)).toString()));
        
	}
	
	
}
